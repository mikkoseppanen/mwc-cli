package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"bitbucket.org/mikkoseppanen/nut"
)

var csvReader io.Reader
var sortBy nut.EmployeesBy

// init gets executed before main. We'll init CLI parameters here.
func init() {
	path := flag.String("csv", "-", "CSV data path. '-' can be used to read from standard input.")
	sorting := flag.String("sort", "id", "Sorting method. Valid values are: 'id', 'name'")

	flag.Parse()

	// Check sorting.
	switch {
	case strings.EqualFold(*sorting, "id"):
		sortBy = nut.EmployeesByID

	case strings.EqualFold(*sorting, "name"):
		sortBy = nut.EmployeesByName

	default:
		log.Fatalf("Unrecognized sort method: '%s'\n", *sorting)
	}

	// Check input path. We do this last to avoid opening files unnecessarily.
	switch {
	case *path == "-":
		csvReader = os.Stdin

	default:
		f, err := os.Open(*path)
		if err != nil {
			log.Fatalf("Error opening csv: '%s'\n", err.Error())
		}

		csvReader = f
	}
}

func main() {
	// Init new CSV parser
	p := nut.NewCSVParser(csvReader)

	// Attempt to gather a report
	report, err := nut.GenerateReport(p)
	if err != nil {
		log.Fatalln(err.Error())
	}

	// Sort by selected sorter
	report.Sort(sortBy)

	// And output results
	fmt.Printf("Monthly Wages %s:\n", report.DateString())

	for _, emp := range report.Employees {
		report := emp.Compensation()

		fmt.Printf("%s, %s: $%.02f\n", emp.ID, emp.Name, (float64(report.Wage) / 100.0))
		fmt.Printf(
			"\tHours: regular %s, evening %s, overtime %s, TOTAL: %s\n",
			report.HoursRegular,
			report.HoursEvening,
			report.HoursOvertime,
			report.HoursTotal,
		)
	}
}
