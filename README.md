# Monthly Wage Calculation CLI

CLI application for calculating monthly wages from given CSV data.

Wrapper for [nut](https://bitbucket.org/mikkoseppanen/nut).

Allows for basic sorting of the results as well as reading from both stdin and file.

## Acquiring

`go get -u bitbucket.org/mikkoseppanen/mwc-cli`

## Building

`go build bitbucket.org/mikkoseppanen/mwc-cli`

## Installing

`go install bitbucket.org/mikkoseppanen/mwc-cli`

## Usage

`mwc-cli --help`